class BaseInputClass:
    pass


class ConsoleInputClass(BaseInputClass):
    def get_text(self):
        text = input()
        return text


class CalculateWordsFrequence:
    def __init__(self, text):
        self.text = text

    def get_most_common_words(self):
        self.text = "".join(filter(lambda ch: ch.isalpha() or ch == " ", list(self.text)))
 
        words_list = self.text.split(" ")

        counters_list = {word: words_list.count(word) for word in words_list}
        most_common = sorted(counters_list.items(), key=lambda i: i[1], reverse=True)[:3]

        return dict(most_common)


class BaseOutputClass():
    pass


class ConsoleOutputClass(BaseOutputClass):
    def __init__(self, freq):
        self.freq = freq

    def print_result(self):
        for word in self.freq:
            print(f"'{word}': {self.freq[word]}")


if __name__ == "__main__":
    c = ConsoleInputClass()
    text = c.get_text()

    c = CalculateWordsFrequence(text)
    freq = c.get_most_common_words()

    c = ConsoleOutputClass(freq)
    c.print_result()
